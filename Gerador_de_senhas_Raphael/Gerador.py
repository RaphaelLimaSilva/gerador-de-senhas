import hashlib
import datetime
import random
import string


def gerar_senha():
    letras = random.choices(string.ascii_lowercase, k=10)
    letras = "".join(letras)
    data = datetime.datetime.now()
    data = data.strftime("%H:%M:%S")
    tudo = bytes(data+letras, 'utf-8')
    senha = hashlib.sha256()
    senha.update(tudo)

    return(senha.hexdigest())


